$(document).ready(function () {
  "usetrict"
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gBASE_URL = "https://62442d9d3da3ac772b0c50eb.mockapi.io/api/v1/";
  const gPRODUCT_COLUMNS = ["id", "name", "email", "message", "createDate", "action"]
  const gCOLUMN_STT = 0
  const gCOLUMN_NAME = 1
  const gCOLUMN_EMAIL = 2
  const gCOLUMN_MESSAGE = 3
  const gCOLUMN_CREATE_DAY = 4
  const gCOLUMN_ACTION = 5
  var gStt = 0
  var gTable = $("#example2").DataTable({
    // Phân trang
    paging: true,
    // Tìm kiếm
    searching: false,
    // Sắp xếp
    ordering: false,
    // Số bản ghi phân trang
    lengthChange: false,
    columns: [
      { data: gPRODUCT_COLUMNS[gCOLUMN_STT] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_NAME] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_EMAIL] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_MESSAGE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_CREATE_DAY] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_ACTION] }


    ],
    columnDefs: [
      {
        targets: gCOLUMN_STT,
        render: renderStt
      },
      {
        targets: gCOLUMN_ACTION,
        defaultContent: `<button class="btn btn-edit">
        <i class="fa-solid fa-pen-to-square"></i>
        </button>
        <button class="btn btn-delete">
        <i class="fa-solid fa-trash-can"></i>
        </button>`
      },
    ]
  })
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading()
  // gán sự kiện cho nút Thêm khóa học
  $(document).on("click", "#btn-add-contact", function () {
    $("#add-modal").modal("show")
  })
  // Gán sự kiện cho nút xác nhận thêm khóa học
  $(document).on("click", "#btn-confirm-add-course", function () {
    onBtnAddContactClick()
  })
  // Gán sự kiện cho nút edit
  $(document).on("click", ".btn-edit", function () {
    onBtnEditClick(this)
  })
  // Gán sự kiện cho nút update product trong modal edit
  $(document).on("click", "#btn-confirm-update-contact", function () {
    onBtnUpdateContactClick()
  })
  // Gán sự kiện cho nút Delete
  $(document).on("click", ".btn-delete", function () {
    onBtnDeleteClick(this)
  })
  // Gán sự kiện cho nút confirm delete
  $(document).on("click", "#btn-confirm-delete-contact", function () {
    onBtnConfirmDeleteClick()
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Gọi Api
    processApiToLoadListContact()
  }
  function onBtnAddContactClick() {
    var vContact = {
      name: "",
      email: "",
      message: "",
      createDate: ""
    }
    // B1 Thu thập dữ liệu
    getDataToAddContact(vContact)
    // B2 Kiểm tra dữ liệu
    var vValidate = isContactDataValidate(vContact)
    if (vValidate == true) {
      //B3 Gọi Api để thêm vào danh sách
      processApiToAddContact(vContact)
    }

  }
  function onBtnUpdateContactClick() {
    var vContact = {
      name: "",
      email: "",
      message: "",
      createDate: ""
    }
    // B1 Thu thập dữ liệu
    getDataToUpdateContact(vContact)
    // B2 kiểm tra dữ liệu
    var vValidate = isContactDataValidate(vContact)
    if (vValidate == true) {
      // B3 Gọi Api và xử lý hiển thị
      processApiToUpdateContact(vContact)
    }

  }
  function onBtnConfirmDeleteClick(paramButton) {
    // B1 Lấy Id của contact cần xóa
    var vCourseID = $("#btn-confirm-delete-contact").attr("courseId")
    // B2 Kiểm tra dữ liệu[không có]
    // B3 Gọi Api xóa khóa học
    processApiToDeleteContact(vCourseID)
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm render Cột số thứ tự
  function renderStt() {
    gStt++
    return gStt
  }
  // Hàm gọi Api để lấy dữ liệu từ server cho việc hiển thị ra site
  function processApiToLoadListContact() {
    gStt = 0
    $.ajax({
      url: gBASE_URL + "contacts/",
      type: "GET",
      success: function (responseObject) {
        displayDataToTable(responseObject)
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm hiển thị table
  function displayDataToTable(paramObject) {
    gTable.clear();
    gTable.rows.add(paramObject);
    gTable.draw();
  }
  // Hàm thu thập dữ liệu để thêm mới liên hệ
  function getDataToAddContact(paramObject) {
    paramObject.name = $.trim($("#input-add-name").val())
    paramObject.email = $.trim($("#input-add-email").val())
    paramObject.message = $.trim($("#input-add-message").val())
    paramObject.createDate = getCurrentDay()
    // console.log(paramObject)
  }
  // Hàm kiểm tra dữ liệu
  function isContactDataValidate(paramObject) {
    if (paramObject.name == "") {
      alert("Hãy nhập tên người liên hệ")
      return false
    }
    if (isEmailValidate(paramObject.email) == false) {
      alert("Email không hợp lệ")
      return false
    }
    if (paramObject.message == "") {
      alert("Hãy nhập lời nhắn")
      return false
    }
    return true
  }
  // Hàm gọi Api để thêm liên hệ
  function processApiToAddContact(paramObject) {
    $.ajax({
      url: gBASE_URL + "contacts/",
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (response) {
        console.log(response)
        // B4: xử lý front-end
        alert(`
            Thêm liên hệ thành công`)
        $("#add-modal").modal("hide")
        onPageLoading()

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
  // Hàm xử lý khi click nút edit
  function onBtnEditClick(paramBtn) {
    $("#edit-modal").modal("show")
    var vRowSelected = $(paramBtn).parents("tr")
    var vDataRow = gTable.row(vRowSelected).data()
    // console.log(vDataRow)
    $("#input-edit-name").val(vDataRow.name)
    $("#input-edit-email").val(vDataRow.email)
    $("#input-edit-message").val(vDataRow.message)
    $("#btn-confirm-update-contact").attr("courseId", vDataRow.id)
  }
  // Hàm thu thập dữ liệu để thêm mới liên hệ
  function getDataToUpdateContact(paramObject) {
    paramObject.name = $.trim($("#input-edit-name").val())
    paramObject.email = $.trim($("#input-edit-email").val())
    paramObject.message = $.trim($("#input-edit-message").val())
    paramObject.createDate = getCurrentDay()
    // console.log(paramObject)
  }
  // Hàm gọi Api để update contacts
  function processApiToUpdateContact(paramObject) {
    $.ajax({
      url: gBASE_URL + "contacts/" + $("#btn-confirm-update-contact").attr("courseId"),
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (responseObject) {
        alert("Update liên hệ thành công")
        // Ẩn modal edit
        $("#edit-modal").modal("hide")
        // Load lại bảng
        processApiToLoadListContact()
        // B4 xử lý hiển thị
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm xử lý khi click nút delete
  function onBtnDeleteClick(paramBtn) {
    $("#delete-confirm-modal").modal("show")
    var vRowSelected = $(paramBtn).parents("tr")
    var vDataRow = gTable.row(vRowSelected).data()
    $("#btn-confirm-delete-contact").attr("courseId", vDataRow.id)
  }
  // Hàm gọi Api xóa liên hệ
  function processApiToDeleteContact(paramContactId) {
    $.ajax({
      url: gBASE_URL + "contacts/" + paramContactId,
      type: "DELETE",
      success: function (response) {
        // Xử lý hiển thị
        alert("xóa liên hệ thành công");
        // Ẩn modal xóa
        $("#delete-confirm-modal").modal("hide")
        // Load lại bảng
        processApiToLoadListContact()
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });

  }
  // hàm lấy ngày hôm nay
  function getCurrentDay() {
    const date = new Date();

    let day = date.getDate();
    let month = date.getMonth() + 1
    if (month < 10) {
      month = `0${(date.getMonth() + 1)}`
    }
    let year = date.getFullYear();

    // This arrangement can be altered based on how we want the date's format to appear.
    let currentDate = `${day}/${month}/${year}`;
    return currentDate // "17-6-2022"
  }
  //Hàm kiểm tra email
  function isEmailValidate(paramEmail) {
    var vRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return vRegex.test(paramEmail)
  }
})